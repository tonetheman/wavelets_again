

from PIL import Image

fname = "20170610_192903_small.jpg"
img = Image.open(fname)

from test_split import split_data

print("get bands",img.getbands())
band = img.getdata(0)
print("len of band",len(band))
current_band = band
while True:
    new_band,details = split_data(current_band,len(current_band))
    print("len of new band",len(new_band))
    if len(new_band)<5:
        break
    current_band = new_band
