
def split_data(band,band_len):
    new_band = []
    details_band = []
    index = 0
    while True:
        # get average of these two pixels
        v1 = band[index]
        v2 = band[index+1]
        index = index + 2
        a = (v1+v2)/2.0
        # new red band half width
        new_band.append(a)
        # save off the details
        details_band.append(v1-a)
        # quit if you need
        if index>=band_len:
            break

    return (new_band,details_band)

def handle_data(starting_data):
    print("starting data",starting_data)
    new_band1, details1 = split_data(starting_data,len(starting_data))
    print("new band 1 and details 1",new_band1, details1)
    new_band2, details2 = split_data(new_band1,len(new_band1))
    print("new band 2 and details 2", new_band2,details2)
    new_band3, details3 = split_data(new_band2,len(new_band2))
    print("new band 3 and details 3", new_band3, details3)
    print("**********************")

if __name__ == "__main__":
    # print(split_data([1,2,3,4,5,6,7,8],8))
    # print(split_data([1,2,3,4,5,6,7,8],4))
    starting_data =[1,2,3,4,5,6,7,8]
    handle_data(starting_data)
    starting_data = [4,1,2,2,9,8,8,2]
    handle_data(starting_data)
    starting_data = [5,4,8,3,3,1,8,6]
    handle_data(starting_data)
