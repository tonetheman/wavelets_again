
- how do you reverse the filter bank?
  - started test_split.py program to test how I am splitting data
  - which looks correct

  - to get new_band from previous filter you do this
    - take detail band and add and subtract from new_band
      - do this for each item in details and matching item in new_band

  - not sure how to get details from previous filter


- DONE: saved as ascii
- DONE: check compression for ascii
- DONE: save for binary
- DONE: check compression for binary
- DONE: switched to samll file (same ratio)
- DONE: use entire file

- change to YCbCr
  - save as ascii
  - check compression here
  - save for binary
  - check compression for binary

  - do subsample for YCbCr
  - save as ascii
  - check compression
  - save for binary
  - check compression for binary (subsample)
