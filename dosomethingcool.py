from PIL import Image

fname = "20170610_192903_small.jpg"
img = Image.open(fname)
print(img)
#
# pixel = img.getpixel((0,0))
# print(pixel)

def save_to_ascii(img):
    outf = open("ascii.txt","w")
    # i indexes height (y value)
    for i in range(img.height):
        # j indexes the width (x value)
        for j in range(img.width):
            pixel = img.getpixel((j,i))
            ts = "{0} {1} {2} ".format(pixel[0],pixel[1],pixel[2])
            outf.write(ts)
        outf.write("\n")
    outf.close()

def save_to_binary(img):
    import struct
    outf = open("binary.bin","wb")
    for i in range(img.height):
        for j in range(img.width):
            pixel = img.getpixel((j,i))
            b = struct.pack("3B",pixel[0],pixel[1],pixel[2])
            # print(len(b))
            outf.write(b)
    outf.close()

def gzip_file(fname,ofname,binary=False):
    import zlib
    if binary==False:
        inf = open(fname,"r")
    else:
        inf = open(fname, "rb")
    ts = inf.read()
    inf.close()
    if binary==False:
        ts = bytearray(ts,"utf-8")
    print("uncompressed size",len(ts))
    for i in range(9):
        res = zlib.compress(ts,i)
        print("compression level", i,
        "compressed size", len(res), len(res)/len(ts))

def rgb_pixel_to_ycbcr_jpeg(p):
    # jpeg version
    r = p[0]
    g = p[1]
    b = p[2]
    y = 0 + (0.299 * r) + (0.587*g) + (0.114*b)
    cb = 128 -(0.168736 * r) - (0.331264*g) + (0.5*b)
    cr = 128+(0.5*r)-(0.418688*g)-(0.081312*b)
    return (y,cb,cr)

def ycbcr_pixel_to_rgb(p):
    y = p[0]
    cb = p[1]
    cr = p[2]
    r = 255/219 * (y-16) + 255/112 * 0.701 * (cr-128)
    g = 255/219 * (y-16) - 255/112*0.886 * 0.114/0.587 * (cb-128)-255/112*0.701*0.299/0.587 * (cr-128)
    b = 255/219 * (y-16) + 255/112*0.886 * (cb-128)
    # data lost here
    if r<0:
        r = 0
    if g<0:
        g = 0
    if b<0:
        b = 0
    return (round(r),round(g),round(b))

def rgb_pixel_to_ycbcr(p):
    # ITU-R BT.601 conversion
    # https://en.wikipedia.org/wiki/YCbCr#ITU-R_BT.601_conversion
    r = p[0]/255.0
    g = p[1]/255.0
    b = p[2]/255.0
    y = 16 + (65.481 * r + 128.553 * g + 24.966 * b)
    cb = 128 + (-37.797 * r -74.203 * g + 112.0 * b)
    cr = 128 + (112.0 * r - 93.786 * g - 18.214 * b)
    return (y,cb,cr)

def convert_to_ycbcr_and_compress_test(img):
    import struct
    outf = open("ycbcr_ascii.txt","w")
    outf_bin = open("ycbcr_binary.bin","wb")

    for i in range(img.height):
        for j in range(img.width):
            pixel = img.getpixel((j,i))
            ycbcr = rgb_pixel_to_ycbcr(pixel)

            # NOTE LOST DATA HERE
            b = struct.pack("3B",int(ycbcr[0]),int(ycbcr[1]),int(ycbcr[2]))
            outf_bin.write(b)

            # NOTE LOST DATA HERE
            ts = "{0} {1} {2} ".format(int(ycbcr[0]),int(ycbcr[1]),int(ycbcr[2]))
            outf.write(ts)
            outf.write("\n")

    outf.close()
    outf_bin.close()

def ycbcr_binary_file_convert(fname):
    inf = open(fname,"rb")
    data = inf.read()
    inf.close()

    # now take the binary data and
    # make a new image with it
    img = Image.new("RGB",(800,600))
    index = 0
    w = 0
    h = 0
    while True:
        r = data[index]
        index = index + 1
        g = data[index]
        index = index + 1
        b = data[index]
        index = index + 1
        # print((w,h))
        p = ycbcr_pixel_to_rgb((r,g,b))
        if (h==258 or h==259) and (w==306 or w==307 or w==308):
            print(w,h,r,g,b,p)

        img.putpixel((w,h),p)
        w = w + 1
        if w>799:
            w = 0
            h = h+ 1
        if h>599:
            break
    img.save("out.png")

def test_conversion():
    p=(255,255,255)
    y=rgb_pixel_to_ycbcr(p)
    p1=ycbcr_pixel_to_rgb(y)
    print(p,y,p1)

    p=(0,0,0)
    y=rgb_pixel_to_ycbcr(p)
    p1=ycbcr_pixel_to_rgb(y)
    print(p,y,p1)

    p=(128,128,128)
    y=rgb_pixel_to_ycbcr(p)
    p1=ycbcr_pixel_to_rgb(y)
    print(p,y,p1)

    p=(12,128,128)
    y=rgb_pixel_to_ycbcr(p)
    p1=ycbcr_pixel_to_rgb(y)
    print(p,y,p1)

    p=(128,12,128)
    y=rgb_pixel_to_ycbcr(p)
    p1=ycbcr_pixel_to_rgb(y)
    print(p,y,p1)

    p=(128,128,12)
    y=rgb_pixel_to_ycbcr(p)
    p1=ycbcr_pixel_to_rgb(y)
    print(p,y,p1)

    p=(16,131,127)
    y=rgb_pixel_to_ycbcr(p)
    p1=ycbcr_pixel_to_rgb(y)
    print(p,y,p1)

    p=(0,0,7)
    y=rgb_pixel_to_ycbcr(p)
    p1=ycbcr_pixel_to_rgb(y)
    print(p,y,p1)

def find_prob_pixels(img):
    print( img.getpixel((307,258)))
    print( img.getpixel((306,258)))

def test():
    print("saving ascii...")
    save_to_ascii(img)
    print("saving binary...")
    save_to_binary(img)
    print("ascii")
    gzip_file("ascii.txt", "outascii.txt", binary=False)
    print("binary")
    gzip_file("binary.bin", "outbinary.bin", binary=True)
    print("converting to ycbcr")
    convert_to_ycbcr_and_compress_test(img)
    print("ycbcr ascii")
    gzip_file("ycbcr_ascii.txt", "outascii.txt", binary=False)
    print("ycbrcr binary")
    gzip_file("ycbcr_binary.bin", "outbinary.bin", binary=True)
    print("testing the ycbcr binary file does it make an image?...")
    ycbcr_binary_file_convert("ycbcr_binary.bin")
    # test_conversion()
    # find_prob_pixels(img)

def w1():

    def split_data(which_band):
        band = img.getdata(which_band)
        new_band = []
        details_band = []
        index = 0
        while True:
            # get average of these two pixels
            v1 = band[index]
            v2 = band[index+1]
            index = index + 2
            a = (v1+v2)/2.0
            # new red band half width
            new_band.append(a)
            # save off the details
            details_band.append(v1-a)
            # quit if you need
            if index>=len(band):
                break

        return (new_band,details_band)


    # get red data
    rband = img.getdata(0)
    new_rband, details_rband = split_data(0)
    print("len of new rband", len(new_rband))
    gband = img.getdata(1)
    new_gband, details_gband = split_data(1)
    bband = img.getdata(2)
    new_bband, details_bband = split_data(2)

    # doing this so I can see the image
    pixels = []
    for i in range(len(new_rband)):
        pixels.append(
        (int(new_rband[i]),
        int(new_gband[i]),
        int(new_bband[i])))

    img2 = Image.new("RGB",(400,600))
    img2.putdata(pixels)
    img2.save("out2.jpg")

    # now write out the binary
    import struct
    outf = open("halfband1.bin","wb")
    outf_a = open("halfband1.asc","w")
    row = 0
    while True:
        for i in range(400):
            # write out the real image data (half size)
            r = new_rband[i+(row*400)]
            g = new_gband[i+(row*400)]
            b = new_bband[i+(row*400)]

            # save it in ascii too so I can see it
            outf_a.write("{0},{1},{2} ".format(r,g,b))

            # these are all positive ints? is that True
            # the int part i mean... they are averages grrrrr
            bytes = struct.pack("3B",int(r),int(g),int(b))
            outf.write(bytes)

        # note here I wrote signed... these can be negative
        # guaranteed to have negative in the mix
        for i in range(400):
            rd = details_rband[i+(400*row)]
            gd = details_gband[i+(400*row)]
            bd = details_bband[i+(400*row)]

            # write in ascii too so I can debug
            outf_a.write("{0},{1},{2} ".format(rd,gd,bd))

            # this is not correct really
            # these cannot be integers....
            # so losing data bad here
            bytes = struct.pack("3b",int(rd),int(gd),int(bd))
            outf.write(bytes)

        # done with a row for the debug file
        outf_a.write("\n")

        # done with the row
        row = row + 1

        if row>=600:
            break

    # close it down
    outf_a.close()
    outf.close()

save_to_ascii(img)
w1()
gzip_file("binary.bin","out.out",binary=True)
gzip_file("halfband1.bin","out.out",binary=True)
